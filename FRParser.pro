#-------------------------------------------------
#
# Project created by QtCreator 2016-03-22T11:22:12
#
#-------------------------------------------------

QT       += core gui xml

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = FRParser
TEMPLATE = app
CONFIG += c++11

SOURCES += \
    controlviewwidget.cpp \
    main.cpp \
    mainwidget.cpp \
    formatgraphscene.cpp \
    gziparchive.cpp \
    reportbaseobject.cpp \
    reporttextobject.cpp \
    reportpictureobject.cpp \
    reportpageobject.cpp

HEADERS  += \
    controlviewwidget.h \
    mainwidget.h \
    formatgraphscene.h \
    gziparchive.h \
    reportbaseobject.h \
    reporttextobject.h \
    reportpictureobject.h \
    reportpageobject.h
    
FORMS    += \
    mainwidget.ui

LIBS += \
 -lz

RESOURCES += \
    img.qrc

