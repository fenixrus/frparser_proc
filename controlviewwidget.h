#ifndef CONTROLVIEWWIDGET_H
#define CONTROLVIEWWIDGET_H

#include "mainwidget.h"//Класс главного виджета
#include "formatgraphscene.h"

#include <QObject>//Главный класс для Qt от которого наследуются многие другие, включая этот.
#include <QDebug>//Подключения класса для писания отладочной информации в консоль
#include <QScreen>//Подключения класса для управление экраном(мы будем получать размеры экрана)
#include <QFileDialog>
#include <QApplication>
#include <QGraphicsView>

class ControlViewWidget : public QObject
{
    Q_OBJECT //Позволяет использовать Qt MOC в своём проекте
public://Начало объявления публичных полей/методов
    explicit ControlViewWidget(QObject *parent = nullptr);//конструктор главного класса с указанием родителя(по умолчанию 0 = нет родителя)
    ~ControlViewWidget();//деструктор главного класса
    //
private slots://
    void slotOpenTemplate();
    void slotFinishedFormat();
    //
private:
    MainWidget *mainWindow;//Стартовый виджет
    QGraphicsView *documentViewWindow;//Виджет показа
    QGraphicsScene *scene;
    QStandardItemModel *model;
    //
};

#endif // CONTROLVIEWWIDGET_H
