#include "formatgraphscene.h"

FormatGraphScene::FormatGraphScene(QObject *parent,
                                   QGraphicsScene *scene,
                                   float ArgPixelPerMm,
                                   QStandardItemModel *ArgModel) :
    QObject(parent),
    graphicsScene(scene),
    pixelPerMm(ArgPixelPerMm),
    model(ArgModel)
{
    _ "FormatGraphScene";
}

FormatGraphScene::~FormatGraphScene()
{
    _ "~FormatGraphScene";
}

void FormatGraphScene::formation(QString reportFileName)
{
    _ "---------------------------------------";
    _  reportFileName;
    _  "---------------------------------------";

    QFile fileInput(reportFileName);
    if(!fileInput.open(QIODevice::ReadOnly)) return;

    QByteArray *output = new QByteArray;

    GZipArchive::gzipDecompress(fileInput.readAll(), output);
    fileInput.close();

    QDomDocument domDoc;

    if(!domDoc.setContent(*output)) return;

    QHash <QString,QString> dictionaryHash;
    QDomNode dictionaryNode = domDoc.elementsByTagName("dictionary").at(0);

    QStandardItem *parentItem = new QStandardItem("dictionary");

    model->appendRow(parentItem);
    QDomNode n = dictionaryNode.firstChild();

    while(!n.isNull()) {
        parentItem->appendRow(new QStandardItem(n.nodeName() + " = " + n.toElement().attribute("name")));
        dictionaryHash.insert(n.nodeName(), n.toElement().attribute("Name"));
        n = n.nextSibling();
    }
    //_ dictionaryHash;

    QDomNode sourcepages = domDoc.elementsByTagName("sourcepages").at(0);


    QVector< QImage > blobstoreElements;

    QDomNode nextPage = sourcepages.firstChild();
    QVector <QDomNode> dataInPages;

    while(!nextPage.isNull()) { //Перебор страниц шаблона(sourcepages)
        QDomNode pages = domDoc.elementsByTagName("pages").at(0);

        QDomNode nextPageInPages = pages.firstChild();

        while(!nextPageInPages.isNull()) { //Перебор страниц c данными для текущей страницы(по Name)

            QString name = nextPageInPages.nodeName();
            QString nameAtr = nextPage.toElement().attribute("Name").toLower();
            _ name << "==" << nameAtr;

            if (name == nameAtr)
                dataInPages.push_back(nextPageInPages);

            nextPageInPages = nextPageInPages.nextSibling();
        }

        ReportPageObject reportPage(graphicsScene, &dictionaryHash, &nextPage, &dataInPages, &blobstoreElements);
        reportPage.parsNode();

        nextPage = nextPage.nextSibling();
    }

    QStandardItem *parentItemSourcepages = new QStandardItem("sourcepages");

    model->appendRow(parentItemSourcepages);
    recursiveParsing(sourcepages, parentItemSourcepages);
    bandNodes.insert("sourcepages", parentItemSourcepages);

    delete output;
    emit signalFinishedFormat();
}

void FormatGraphScene::recursiveParsing(QDomNode parentXmlModel, QStandardItem *parentItemModel)
{
    QDomNode n = parentXmlModel.firstChild();

    while(!n.isNull()) {
        _ n.nodeName() <<  "  =  " << n.toElement().attribute("Name") << "parent = " << parentXmlModel.nodeName();
        QStandardItem *itemModel = new QStandardItem(n.nodeName() + " = " + n.toElement().attribute("Name"));
        if (!n.firstChild().isNull())//Бэнд
        {
            parentItemModel->appendRow(itemModel);
            bandNodes.insert(n.toElement().attribute("Name"), itemModel);
            recursiveParsing(n, itemModel);
        }
        else
            bandNodes.value(n.parentNode().toElement().attribute("Name"))->appendRow(itemModel);
        n = n.nextSibling();
    }
}
