#ifndef FORMATGRAPHSCENE_H
#define FORMATGRAPHSCENE_H

#define _ qDebug() <<

#include "gziparchive.h"
#include "reportpageobject.h"

#include <QObject>
#include <QDebug>
#include <QFile>
#include <QGraphicsScene>
#include <QByteArray>
#include <QDomNode>
#include <QStandardItem>

class FormatGraphScene : public QObject
{
    Q_OBJECT
public:
    explicit FormatGraphScene(QObject *parent = nullptr,
                              QGraphicsScene *scene = nullptr,
                              float ArgPixelPerMm = 3.7,
                              QStandardItemModel *ArgModel = nullptr);
    void formation(QString reportFileName);
    ~FormatGraphScene();

signals:
    void signalFinishedFormat();

public slots:

private:
    QGraphicsScene *graphicsScene;
    float pixelPerMm;
    QStandardItemModel *model;
    QHash<QString, QStandardItem *> bandNodes;

    void recursiveParsing(QDomNode parentXmlModel, QStandardItem *parentItemModel);
};

#endif // FORMATGRAPHSCENE_H
