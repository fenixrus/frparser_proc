#ifndef GZIPARCHIVE_H
#define GZIPARCHIVE_H

#define GZIP_WINDOWS_BIT 15 + 16
#define GZIP_CHUNK_SIZE 32 * 1024

#include <QObject>
#include <zlib.h>

class GZipArchive : public QObject
{
    Q_OBJECT
public:
    explicit GZipArchive(QObject *parent = 0);
    static bool gzipDecompress(QByteArray input, QByteArray *output);

signals:

public slots:
};

#endif // GZIPARCHIVE_H
