#ifndef MAINWIDGET_H
#define MAINWIDGET_H

#include <QWidget>
#include <QStandardItemModel>

namespace Ui {
class MainWidget;
}

class MainWidget : public QWidget
{
    Q_OBJECT

public:
    explicit MainWidget(QWidget *parent = 0);
    ~MainWidget();
    void setFileNameText(QString fileName);
    void setModelTreeView(QStandardItemModel *model);
    bool getShowBorders();
    bool getUsingStandartTemplate();
signals:
    void signalOpenTemplate();

private slots:

private:
    Ui::MainWidget *ui;
};

#endif // MAINWIDGET_H
