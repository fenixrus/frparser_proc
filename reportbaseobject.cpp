#include "reportbaseobject.h"

ReportBaseObject::ReportBaseObject(
        QGraphicsScene *argGraphicsScene,
        QHash<QString, QString> *argDictionary,
        QDomNode *argSource,
        QVector<QDomNode> *argData,
        QVector<QImage> *argBlobstoreElements)
{
    graphicsScene=argGraphicsScene;
    dictionary=argDictionary;
    source=argSource;
    data=argData;
    blobstoreElements=argBlobstoreElements;
}

ReportBaseObject::~ReportBaseObject()
{

}

void ReportBaseObject::parsNode()
{

}

void ReportBaseObject::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{

}
