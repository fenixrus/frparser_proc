#ifndef REPORTBASEOBJECT_H
#define REPORTBASEOBJECT_H

#include <QObject>
#include <QGraphicsItem>
#include <QtXml>

class ReportBaseObject : public QObject,  public QGraphicsItem
{
    Q_OBJECT
public:
    ReportBaseObject(
            QGraphicsScene *argGraphicsScene = nullptr,
            QHash<QString,QString> *argDictionary = nullptr,
            QDomNode *argSource = nullptr,
            QVector<QDomNode> *argData = nullptr,
            QVector<QImage> *argBlobstoreElements = nullptr);
    virtual ~ReportBaseObject();
    virtual void parsNode();
    virtual void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
protected:
    float leftElem;
    float topElem;
    float widthElem;
    float heightElem;
    QGraphicsScene *graphicsScene;
    QHash<QString,QString> *dictionary;
    QDomNode *source;
    QVector<QDomNode> *data;
    QVector<QImage> *blobstoreElements;
private:

};

#endif // REPORTBASEOBJECT_H
