#include "reportpageobject.h"

ReportPageObject::ReportPageObject(
        QGraphicsScene *argGraphicsScene,
        QHash<QString, QString> *argDictionary,
        QDomNode *argSource,
        QVector<QDomNode> *argData,
        QVector<QImage> *argBlobstoreElements)
{
    graphicsScene=argGraphicsScene;
    dictionary=argDictionary;
    source=argSource;
    ReportBaseObject::data=argData;
    blobstoreElements=argBlobstoreElements;

}

void ReportPageObject::parsNode()
{

}

QRectF ReportPageObject::boundingRect() const
{
    return QRectF();
}

void ReportPageObject::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{

}
