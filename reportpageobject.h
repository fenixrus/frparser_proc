#ifndef REPORTPAGEOBJECT_H
#define REPORTPAGEOBJECT_H

#include "reportbaseobject.h"

class ReportPageObject : public ReportBaseObject
{
    Q_OBJECT
public:
    ReportPageObject(QGraphicsScene *argGraphicsScene = nullptr,
                     QHash<QString, QString> *argDictionary = nullptr,
                     QDomNode *argSource = nullptr,
                     QVector<QDomNode> *argData = nullptr,
                     QVector< QImage > *argBlobstoreElements = nullptr);
    void parsNode();
    QRectF boundingRect() const;
protected:

private:
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
};

#endif // REPORTPAGEOBJECT_H
