#include "reportpictureobject.h"

ReportPictureObject::ReportPictureObject(
        QGraphicsScene *argGraphicsScene,
        QHash<QString, QString> *argDictionary,
        QDomNode *argSource,
        QVector<QDomNode> *argData,
        QVector<QImage> *argBlobstoreElements)
{
    graphicsScene=argGraphicsScene;
    dictionary=argDictionary;
    source=argSource;
    ReportBaseObject::data=argData;
    blobstoreElements=argBlobstoreElements;
}

void ReportPictureObject::parsNode()
{

}

QRectF ReportPictureObject::boundingRect() const
{
    return QRectF();
}

void ReportPictureObject::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{

}
