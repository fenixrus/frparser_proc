#ifndef REPORTPICTUREOBJECT_H
#define REPORTPICTUREOBJECT_H

#include "reportbaseobject.h"

class ReportPictureObject : public ReportBaseObject
{
    Q_OBJECT
public:
    ReportPictureObject(
            QGraphicsScene *argGraphicsScene = nullptr,
            QHash<QString, QString> *argDictionary = nullptr,
            QDomNode *argSource = nullptr,
            QVector <QDomNode> *argData = nullptr,
            QVector< QImage > *argBlobstoreElements = nullptr);
    void parsNode();
    QRectF boundingRect() const;
protected:
    int itemIndex;
private:
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
};

#endif // REPORTPICTUREOBJECT_H
