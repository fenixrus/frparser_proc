#include "reporttextobject.h"

ReportTextObject::ReportTextObject(
        QGraphicsScene *argGraphicsScene,
        QHash<QString, QString> *argDictionary,
        QDomNode *argSource,
        QVector<QDomNode> *argData,
        QVector<QImage> *argBlobstoreElements)
{
    graphicsScene=argGraphicsScene;
    dictionary=argDictionary;
    source=argSource;
    ReportBaseObject::data=argData;
    blobstoreElements=argBlobstoreElements;

}

void ReportTextObject::parsNode()
{

}

QRectF ReportTextObject::boundingRect() const
{
    return QRectF();
}

void ReportTextObject::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{

}
