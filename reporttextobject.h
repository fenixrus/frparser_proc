#ifndef REPORTTEXTOBJECT_H
#define REPORTTEXTOBJECT_H

#include "reportbaseobject.h"

class ReportTextObject : public ReportBaseObject
{
    Q_OBJECT
public:
    ReportTextObject(
            QGraphicsScene *argGraphicsScene = nullptr,
            QHash<QString, QString> *argDictionary = nullptr,
            QDomNode *argSource = nullptr,
            QVector<QDomNode> *argData = nullptr,
            QVector<QImage> *argBlobstoreElements = nullptr);

    void parsNode();
    QRectF boundingRect() const;
protected:
    QString text;
    QColor color;
    int flags;
private:
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
  {
      ReportBaseObject:paint(painter, option, widget);

   }
};

#endif // REPORTTEXTOBJECT_H
